"""
 Very simple SMTP server to process image stills received from ip-cams

 Franco Fiorese <franco.fiorese@gmail.com>
 May 2017

"""

from datetime import datetime
import asyncore, sys, email, os
from smtpd import SMTPServer

from minio import Minio
from minio.error import (InvalidResponseError, S3Error)

import logging
import logging.handlers
import traceback

log_levels = { "debug": logging.DEBUG, "info": logging.INFO, "warning": logging.WARNING, "error": logging.ERROR, "critical": logging.CRITICAL }

def loglevel(lev):
    try:
        return log_levels[lev]
    except KeyError as ex:
        return log_levels["info"]


class Attachment(object):
    def __init__(self):
        self.data = None;
        self.content_type = None;
        self.size = None;
        self.name = None;

class EmailServer(SMTPServer):
    def __init__(self, loc_addr, endpoint, access_key, secret_key, region, secure, bucket, folder, logger):
        super().__init__(localaddr=loc_addr, remoteaddr=None, decode_data=True)
        self.mc = Minio(endpoint=endpoint,
                        access_key=access_key,
                        secret_key=secret_key,
                        region=region,
                        secure=secure)
        self.bucket = bucket
        self.folder = folder
        self.logger = logger

    def archive_photo(self, user, fspec):
        fdest = f"{self.folder}/{user}/{datetime.now().strftime('%Y-%m-%d')}/{fspec}"
        self.logger.info(f'uploading {fspec} as {fdest} to {user}')
        try:
            self.mc.fput_object(self.bucket, fdest, fspec)
            os.remove(fspec)
            self.logger.info(f'S-upload {fspec} successful')
        except InvalidResponseError as err:
            self.logger.error(f'S-Cannot upload file {fspec}')
        except FileNotFoundError:
            self.logger.error(f'S-File {fspec} no more present')
        except Exception as exc:
            ex_trace = traceback.format_exc()
            self.logger.error(f"S-Upload exception: {ex_trace}")
    
    def parse_attachment(self, message_part):
        content_disposition = message_part.get("Content-Disposition", None);
        if content_disposition:
            dispositions = content_disposition.strip().split(";");
            if bool(content_disposition and dispositions[0].lower() == "attachment"):
                attachment = Attachment();
                attachment.data = message_part.get_payload(decode=True);
                attachment.content_type = message_part.get_content_type();
                if attachment.content_type == "image/jpeg":
                    attachment.size = len(attachment.data);
                    attachment.name = message_part.get_filename();
                    return attachment;

        return None;

    def process_message(self, peer, mailfrom, rcpttos, data):

        msg = email.message_from_string(data);
        print(msg["Subject"])
        attachments = list();

        print("email from: %s" % mailfrom)
        user, domain = mailfrom.split('@')
        print("user=%s domain=%s" % (user, domain))

        if (msg.is_multipart()):
            for part in msg.walk():
                att = self.parse_attachment(part);
                if (att):
                    attachments.append(att);
        else:
            print("this is not a  multipart message")

        nx = 1
        for att in attachments:
            fspec = f"{datetime.now().strftime('%H-%M-%S')}_{nx}.jpg"
            file = open(fspec, 'wb');
            file.write(att.data);
            file.close();
            self.archive_photo(user, fspec)
            nx += 1

def run():
    epoint = os.environ.get("OBJSERV_ENDPOINT")
    akey = os.environ.get("OBJSERV_AKEY")
    skey = os.environ.get("OBJSERV_SKEY")
    bucket = os.environ.get("OBJSERV_BUCKET")
    folder = os.environ.get("OBJSERV_FOLDER")
    region = os.environ.get("OBJSERV_REGION")
    secured = os.environ.get("OBJSERV_USESSL") == "true"

    logger = logging.getLogger("ipcam_am")
    logger.propagate = False
    logger.setLevel(loglevel("debug"))
    fh = logging.StreamHandler(sys.stdout)
    logger.addHandler(fh)
    es = EmailServer(('0.0.0.0', 1025), epoint, access_key=akey, secret_key=skey, region=region, secure=secured, bucket=bucket, folder=folder, logger=logger)
    print("Doorbell email alert server started")
    try:
        asyncore.loop()
    except KeyboardInterrupt:
        pass

if __name__ == '__main__':
    run()
