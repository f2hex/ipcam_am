import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.application import MIMEApplication
from os.path import basename
  
#Establish SMTP Connection
srv = smtplib.SMTP('localhost', 1025) 
#srv.set_debuglevel(True)  

#Login Using Your Email ID & Password
#srv.login("franco", "mypass")
  
#To Create Email Message in Proper Format
msg = MIMEMultipart()

#Setting Email Parameters
msg['From'] = "dorbell@f2hex.net"
msg['To'] = "franco@localhost"
msg['Subject'] = "test"

#Email Body Content
message = """
<h1>Alert test</h1>
"""

#Add Message To Email Body
msg.attach(MIMEText(message, 'html'))

for nx in range(1,4):
    fspec = f"polifemo-{nx}.jpg"
    finp = open(fspec, "rb")
    part = MIMEImage(finp.read())
    part.add_header('Content-Disposition', 'attachment; filename="' + basename(fspec) + '"')
    msg.attach(part)
    print(f"Added attchment: {fspec}")

#To Send the Email
srv.send_message(msg)
  
#Terminating the SMTP Session
srv.quit()
