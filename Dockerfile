# ; -*- mode: dockerfile;-*-
FROM python:3.10.9-slim-bullseye

MAINTAINER Franco Fiorese <franco@f2hex.net>

ENV WGROUP_ID=9001
ENV WGROUP=wgroup
ENV WUSER_ID=9001
ENV WUSER=wuser

RUN    apt-get update && apt-get upgrade -y \
    && rm -rf /var/cache/apt/*

RUN    groupadd -g "${WGROUP_ID}" "${WGROUP}"\
    && useradd -g "${WGROUP}" -u "${WUSER_ID}" -m "${WUSER}" \
    && mkdir /app

COPY ssmtp.py /app
RUN chown -R $WUSER:$WGROUP /app

USER "${WUSER}"

RUN    pip3 install minio

WORKDIR /app
VOLUME /app/img
EXPOSE 1025

CMD ["python3","ssmtp.py"]
